import Vue from 'vue'
import VueRouter from 'vue-router'
import DashBoard from '../views/DashBoard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'dash',
    component: DashBoard
  },
  {
    path: '/user',
    name: 'user',
    component: () => import('../views/UsersView.vue')
  },
  {
    path: '/trainer',
    name: 'trainer',
    component: () => import('../views/TrainerView.vue')
  },
  {
    path: '/gruppen',
    name: 'gruppen',
    component: () => import('../views/GruppenView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
