import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    teilnehmer: [],
    trainerDto:[],
    trainer: [],
    gruppenDto: [],
    gruppen: [],
    raumChoose: [],
    raumChooseName: [],
    kategorieChoose: [],
    kategorieChooseName: [],
    fachChoose: [],
    fachChooseName: [],
    unterrichtBoolean: '',
    trainerListMo:[],
    trainerListDi:[],
    trainerListMi:[],
    trainerListDo:[],
    trainerListFr:[],

  },
  getters: {
  },
  mutations: {
    setTeilnehmer(state, teilnehmer){
      state.teilnehmer = teilnehmer;
    },
    setTrainer(state, trainerDto){
      state.trainer = trainerDto.trainer;
      state.fachChoose = trainerDto.fachChoose;
      state.fachChooseName = trainerDto.fachChooseName;
    },
    setGruppen(state, gruppenDto){
      state.gruppen = gruppenDto.gruppen;
      state.raumChoose = gruppenDto.raumChoose;
      state.raumChooseName = gruppenDto.raumChooseName;
      state.kategorieChoose = gruppenDto.kategorieChoose;
      state.kategorieChooseName = gruppenDto.kategorieChooseName;
      state.fachChoose = gruppenDto.fachChoose;
      state.fachChooseName = gruppenDto.fachChooseName;
    },
    setTrainerListForDays(state, trainerListDays){
      state.trainerListMo = trainerListDays[0]
      state.trainerListDi = trainerListDays[1]
      state.trainerListMi = trainerListDays[2]
      state.trainerListDo = trainerListDays[3]
      state.trainerListFr = trainerListDays[4]
    }
  },
  actions: {
    async getTeilnehmer(store){
      const userResponse = await axios.get('http://localhost:8080/teilnehmer')
      store.commit('setTeilnehmer', userResponse.data)
    },
    async addTeilnehmer(store, teilnehmer){
      store = await axios.post("http://localhost:8080/teilnehmer", teilnehmer)
    },
    async deleteTeilnehmer(store, id){
      store = await axios.delete("http://localhost:8080/teilnehmer/"+ id)
    },
    async updateTeilnehmer(store, teilnehmer){
      store = await axios.put("http://localhost:8080/teilnehmer/"+ teilnehmer.id, teilnehmer)
    },

    async getTrainer(store){
      const userResponse = await axios.get('http://localhost:8080/trainer')
      store.commit('setTrainer', userResponse.data)
    },
    async addTrainer(store, trainer){
      store = await axios.post("http://localhost:8080/trainer", trainer)
    },
    async deleteTrainer(store, id){
      store = await axios.delete("http://localhost:8080/trainer/"+ id)
    },
    async updateTrainer(store, trainer){
      store = await axios.put("http://localhost:8080/trainer/"+ trainer.id, trainer)
    },

    async getGruppen(store){
      const userResponse = await axios.get('http://localhost:8080/gruppen')
      store.commit('setGruppen', userResponse.data)
    },
    async addGruppen(store, gruppen){
      const userResponse = await axios.post('http://localhost:8080/gruppen', gruppen)
      this.state.unterrichtBoolean = userResponse.data;
    },
    async deleteGruppen(store, id){
      store = await axios.delete("http://localhost:8080/gruppen/"+ id)
    },
    async updateGruppen(store, gruppen){
      store = await axios.put("http://localhost:8080/gruppen/"+ gruppen.id, gruppen)
    },
    async getRaumList(store, gruppen){
      const userResponse = await axios.get('http://localhost:8080/gruppenraumlist/' + gruppen.id)
      store.commit('setGruppen', userResponse.data)
    },

    async addTeilnehmToGruppe(store, teilnehmerGruppeAddenRemove){
      store = await axios.put("http://localhost:8080/teilnehmertogruppe/" + teilnehmerGruppeAddenRemove.gruppenId, teilnehmerGruppeAddenRemove)
    },
    async addTrainerToGruppe(store, trainerGruppeAddenRemove){
      store = await axios.put("http://localhost:8080/trainertogruppe/" + trainerGruppeAddenRemove.gruppenId, trainerGruppeAddenRemove)
    },

    async getTrainerListForDays(store, id){
      const userResponse = await axios.get('http://localhost:8080/filteredtrainer/'+id)
      store.commit('setTrainerListForDays', userResponse.data)
    },


  },
  modules: {
  }
})
