# Administration Tool
It´s a administration tool (CRUD). Create students, trainers, groups and courses. For organization of courses.

### Used in this project
- Vue.JS (HTML/CSS/JavaScript), SpringBoot (Java), MySQL


# How to start
- ***MySQL***
- Install ***NodeJS*** & ***NPM***

### Frontend
- Open Project in IDE, run ***npm install*** and ***npm run serve***
- Type in browser ***http://localhost:3000***

### Backend
- Install ***Java 17***
- Open project in ***Intellij***
- Run ***Main.java***
