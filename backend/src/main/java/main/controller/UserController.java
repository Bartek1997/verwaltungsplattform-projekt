package main.controller;

import main.choose.FachChoose;
import main.choose.KategorieChoose;
import main.choose.RaumChoose;
import main.dto.GruppenDto;
import main.dto.TeilnehmerGruppeDto;
import main.dto.TrainerDto;
import main.dto.TrainerGruppeDto;
import main.objects.Gruppen;
import main.objects.Teilnehmer;
import main.objects.Trainer;
import main.repository.GruppenRepository;
import main.repository.TeilnehmerRepository;
import main.repository.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin (origins = "http://localhost:3000")
@RestController
public class UserController {

    @Autowired
    TeilnehmerRepository teilnehmerRepository;

    @Autowired
    TrainerRepository trainerRepository;

    @Autowired
    GruppenRepository gruppenRepository;

    @GetMapping("/teilnehmer")
    public List<Teilnehmer> getTeilnehmer(){
        List<Teilnehmer> teilnehmerList = (List<Teilnehmer>) teilnehmerRepository.findAll();
        return teilnehmerList;
    }

    @PostMapping("/teilnehmer")
    public void addTeilnehmer(@RequestBody Teilnehmer teilnehmer){
        teilnehmerRepository.save(teilnehmer);
    }

    @DeleteMapping("/teilnehmer/{id}")
    public void deleteTeilnehmer(@PathVariable long id){
        teilnehmerRepository.deleteById(id);
    }

    @PutMapping("/teilnehmer/{id}")
    public void updateTeilnehmer (@PathVariable long id, @RequestBody Teilnehmer teilnehmer){
        Teilnehmer teilnehmerEdit = teilnehmerRepository.findById(id).get();
        teilnehmerEdit.setVorname(teilnehmer.getVorname());
        teilnehmerEdit.setNachname(teilnehmer.getNachname());
        teilnehmerRepository.save(teilnehmerEdit);
    }



    @GetMapping("/trainer")
    public TrainerDto getTrainer(){
        return new TrainerDto((List<Trainer>)trainerRepository.findAll(), FachChoose.values());
    }

    @PostMapping("/trainer")
    public void addTainer(@RequestBody Trainer trainer){
        trainerRepository.save(trainer);
    }

    @DeleteMapping("/trainer/{id}")
    public void deleteTrainer(@PathVariable long id){
        trainerRepository.deleteById(id);
    }

    @PutMapping("/trainer/{id}")
    public void updateTrainer (@PathVariable long id, @RequestBody Trainer trainer){
        Trainer trainer1 = trainerRepository.findById(id).get();
        trainer1 = trainer;
        trainer1.setId(id);
        trainerRepository.save(trainer1);
    }



    @GetMapping("/gruppen")
    public GruppenDto getGruppen(){
        List<RaumChoose> raumInGruppe = new ArrayList<>();
        for (Gruppen gruppen : gruppenRepository.findAll()) {
            raumInGruppe.add(gruppen.getRaumChoose());
        }

        List<RaumChoose> raumAlle = new ArrayList<>();
        for (RaumChoose raum : RaumChoose.values()) {
            raumAlle.add(raum);
        }

        for (RaumChoose raum : raumInGruppe) {
            raumAlle.remove(raum);
        }

        RaumChoose[] newArrRaum = raumAlle.toArray(new RaumChoose[0]);

        return new GruppenDto((List<Gruppen>)gruppenRepository.findAll(), newArrRaum, KategorieChoose.values(), FachChoose.values());
    }

    @GetMapping("/gruppenraumlist/{id}")
    public GruppenDto getRaumList(@PathVariable long id){
        List<RaumChoose> raumInGruppe = new ArrayList<>();
        for (Gruppen gruppen : gruppenRepository.findAll()) {
            raumInGruppe.add(gruppen.getRaumChoose());
        }

        List<RaumChoose> raumAlle = new ArrayList<>();
        for (RaumChoose raum : RaumChoose.values()) {
            raumAlle.add(raum);
        }

        for (RaumChoose raum : raumInGruppe) {
            if (raum != gruppenRepository.findById(id).get().getRaumChoose()) {
                raumAlle.remove(raum);
            }

        }
        RaumChoose[] newArrRaum = raumAlle.toArray(new RaumChoose[0]);

        return new GruppenDto((List<Gruppen>)gruppenRepository.findAll(), newArrRaum, KategorieChoose.values(), FachChoose.values());
    }

    @PostMapping("/gruppen")
    public Boolean addGruppen(@RequestBody Gruppen gruppen){
        List<FachChoose> fachChooseList = new ArrayList<>();
        fachChooseList.add(gruppen.getUnterrichtFachMo());
        fachChooseList.add(gruppen.getUnterrichtFachDi());
        fachChooseList.add(gruppen.getUnterrichtFachMi());
        fachChooseList.add(gruppen.getUnterrichtFachDo());
        fachChooseList.add(gruppen.getUnterrichtFachFr());
        List<FachChoose> fachChooseListSW = new ArrayList<>();
        fachChooseListSW.add(FachChoose.WEB);
        fachChooseListSW.add(FachChoose.WEB);
        fachChooseListSW.add(FachChoose.JAVA);
        fachChooseListSW.add(FachChoose.JAVA);
        fachChooseListSW.add(FachChoose.DATENBANK);
        List<FachChoose> fachChooseListNW = new ArrayList<>();
        fachChooseListNW.add(FachChoose.LINUX);
        fachChooseListNW.add(FachChoose.LINUX);
        fachChooseListNW.add(FachChoose.WINDOWS);
        fachChooseListNW.add(FachChoose.WINDOWS);
        fachChooseListNW.add(FachChoose.WINDOWS);
        List<FachChoose> fachChooseListFIA = new ArrayList<>();
        fachChooseListFIA.add(FachChoose.LINUX);
        fachChooseListFIA.add(FachChoose.WINDOWS);
        fachChooseListFIA.add(FachChoose.WEB);
        fachChooseListFIA.add(FachChoose.JAVA);
        fachChooseListFIA.add(FachChoose.DATENBANK);


        fachChooseList = fachChooseList.stream().sorted().toList();

        if (gruppen.getKategorie() == KategorieChoose.SOFTWAREENTWICKLER && fachChooseList.equals(fachChooseListSW)) {
            gruppenRepository.save(gruppen);
            return true;
        }
        else if (gruppen.getKategorie() == KategorieChoose.NETZWERKTECHNIK && fachChooseList.equals(fachChooseListNW)) {
            gruppenRepository.save(gruppen);
            return true;
        }
        else if (gruppen.getKategorie() == KategorieChoose.FIA && fachChooseList.equals(fachChooseListFIA)){
            gruppenRepository.save(gruppen);
            return true;
        }
        else {
            return false;
        }
    }

    @DeleteMapping("/gruppen/{id}")
    public void deleteGruppen(@PathVariable long id){
        gruppenRepository.deleteById(id);
    }

    @PutMapping("/gruppen/{id}")
    public void updateGruppen (@PathVariable long id, @RequestBody Gruppen gruppen){
        Gruppen gruppen1 = gruppenRepository.findById(id).get();
        gruppen1 = gruppen;
        gruppen1.setId(id);
        gruppenRepository.save(gruppen1);
    }



    @PutMapping("/teilnehmertogruppe/{id}")
    public void addTeilnehmerToGruppe(@PathVariable long id, @RequestBody TeilnehmerGruppeDto ids){
        for (long teilnehmerId : ids.getTeilnehmerAddId()) {
            Teilnehmer teilnehmerNew = teilnehmerRepository.findById(teilnehmerId).get();
            teilnehmerNew.setGruppen(gruppenRepository.findById(id).get());
            teilnehmerRepository.save(teilnehmerNew);
        }
        for (long teilnehmerId : ids.getTeilnehmerRemoveId()) {
            Teilnehmer teilnehmerNew = teilnehmerRepository.findById(teilnehmerId).get();
            teilnehmerNew.setGruppen(null);
            teilnehmerRepository.save(teilnehmerNew);
        }
    }

    @PutMapping("/trainertogruppe/{id}")
    public void addTrainerToGruppe(@PathVariable long id, @RequestBody TrainerGruppeDto ids){
        gruppenRepository.findById(id).get().setTrainerMo(null);
        gruppenRepository.findById(id).get().setTrainerDi(null);
        gruppenRepository.findById(id).get().setTrainerMi(null);
        gruppenRepository.findById(id).get().setTrainerDo(null);
        gruppenRepository.findById(id).get().setTrainerFr(null);
        if (ids.getTrainerAddId().get(0) != 0) {
            gruppenRepository.findById(id).get().setTrainerMo(trainerRepository.findById(ids.getTrainerAddId().get(0)).get());
        }
        if (ids.getTrainerAddId().get(1) != 0) {
            gruppenRepository.findById(id).get().setTrainerDi(trainerRepository.findById(ids.getTrainerAddId().get(1)).get());
        }
        if (ids.getTrainerAddId().get(2) != 0) {
            gruppenRepository.findById(id).get().setTrainerMi(trainerRepository.findById(ids.getTrainerAddId().get(2)).get());
        }
        if (ids.getTrainerAddId().get(3) != 0) {
            gruppenRepository.findById(id).get().setTrainerDo(trainerRepository.findById(ids.getTrainerAddId().get(3)).get());
        }
        if (ids.getTrainerAddId().get(4) != 0) {
            gruppenRepository.findById(id).get().setTrainerFr(trainerRepository.findById(ids.getTrainerAddId().get(4)).get());
        }
        gruppenRepository.save(gruppenRepository.findById(id).get());
    }


    @GetMapping("/filteredtrainer/{id}")
    public List<List> getfilteredtrainer(@PathVariable long id){
        List<Trainer> trainerListMo = new ArrayList<>();
        List<Trainer> trainerListDi = new ArrayList<>();
        List<Trainer> trainerListMi = new ArrayList<>();
        List<Trainer> trainerListDo = new ArrayList<>();
        List<Trainer> trainerListFr = new ArrayList<>();


        trainerRepository.findAll().forEach(trainer -> {
            List<Boolean> newArbeitsTage = getArbeitsTageTrainer(trainer);

            if (trainer.getFach() == gruppenRepository.findById(id).get().getUnterrichtFachMo() && newArbeitsTage.get(0)) {
                boolean check = true;
                for (Gruppen gruppen : gruppenRepository.findAll()) {
                    if (gruppen.getTrainerMo() == trainer) {
                        check = false;
                    }
                }
                if (check || gruppenRepository.findById(id).get().getTrainerMo() == trainer) {
                    trainerListMo.add(trainer);
                }
            }
            if (trainer.getFach() == gruppenRepository.findById(id).get().getUnterrichtFachDi() && newArbeitsTage.get(1)) {
                boolean check = true;
                for (Gruppen gruppen : gruppenRepository.findAll()) {
                    if (gruppen.getTrainerDi() == trainer) {
                        check = false;
                    }
                }
                if (check || gruppenRepository.findById(id).get().getTrainerDi() == trainer) {
                    trainerListDi.add(trainer);
                }
            }
            if (trainer.getFach() == gruppenRepository.findById(id).get().getUnterrichtFachMi() && newArbeitsTage.get(2)) {
                boolean check = true;
                for (Gruppen gruppen : gruppenRepository.findAll()) {
                    if (gruppen.getTrainerMi() == trainer) {
                        check = false;
                    }
                }
                if (check || gruppenRepository.findById(id).get().getTrainerMi() == trainer) {
                    trainerListMi.add(trainer);
                }
            }
            if (trainer.getFach() == gruppenRepository.findById(id).get().getUnterrichtFachDo() && newArbeitsTage.get(3)) {
                boolean check = true;
                for (Gruppen gruppen : gruppenRepository.findAll()) {
                    if (gruppen.getTrainerDo() == trainer) {
                        check = false;
                    }
                }
                if (check || gruppenRepository.findById(id).get().getTrainerDo() == trainer) {
                    trainerListDo.add(trainer);
                }
            }
            if (trainer.getFach() == gruppenRepository.findById(id).get().getUnterrichtFachFr() && newArbeitsTage.get(4)) {
                boolean check = true;
                for (Gruppen gruppen : gruppenRepository.findAll()) {
                    if (gruppen.getTrainerFr() == trainer) {
                        check = false;
                    }
                }
                if (check || gruppenRepository.findById(id).get().getTrainerFr() == trainer) {
                    trainerListFr.add(trainer);
                }
            }
        });


        List<List> listOfList = new ArrayList<>();
        listOfList.add(trainerListMo);
        listOfList.add(trainerListDi);
        listOfList.add(trainerListMi);
        listOfList.add(trainerListDo);
        listOfList.add(trainerListFr);
        return listOfList;
    }

    private List<Boolean> getArbeitsTageTrainer(Trainer trainer){
        List<Boolean> newArbeitsTage = new ArrayList<>();
        trainer.getArbeitstage().forEach(s -> {
            if (s == null) {
                s = false;
                newArbeitsTage.add(s);
            }
            else{
                newArbeitsTage.add(s);
            }
        });
        while (newArbeitsTage.size() <=5){
            newArbeitsTage.add(false);
        }
        newArbeitsTage.remove(0);

        return newArbeitsTage;
    }




}
