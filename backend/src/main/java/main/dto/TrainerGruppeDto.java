package main.dto;

import java.util.List;

public class TrainerGruppeDto {
    private long gruppenId;
    private List<Long> trainerAddId;

    public TrainerGruppeDto(long gruppenId, List<Long> trainerAddId) {
        this.gruppenId = gruppenId;
        this.trainerAddId = trainerAddId;
    }

    public long getGruppenId() {
        return gruppenId;
    }

    public void setGruppenId(long gruppenId) {
        this.gruppenId = gruppenId;
    }

    public List<Long> getTrainerAddId() {
        return trainerAddId;
    }

    public void setTrainerAddId(List<Long> trainerAddId) {
        this.trainerAddId = trainerAddId;
    }
}
