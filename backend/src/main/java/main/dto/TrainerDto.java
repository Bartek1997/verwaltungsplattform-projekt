package main.dto;

import main.choose.FachChoose;
import main.choose.KategorieChoose;
import main.choose.RaumChoose;
import main.objects.Gruppen;
import main.objects.Trainer;

import java.util.List;

public class TrainerDto {
    private List<Trainer> trainer;

    private FachChoose[] fachChoose;
    private String[] fachChooseName;

    public TrainerDto(List<Trainer> trainer, FachChoose[] fachChoose) {
        this.trainer = trainer;
        this.fachChoose = fachChoose;
        this.fachChooseName = getFachChooseName();
    }

    public List<Trainer> getTrainer() {
        return trainer;
    }

    public void setTrainer(List<Trainer> trainer) {
        this.trainer = trainer;
    }

    public FachChoose[] getFachChoose() {
        return fachChoose;
    }

    public String[] getFachChooseName() {
        String[] fachhArrayName = new String[fachChoose.length];
        for (int i = 0; i < fachhArrayName.length; i++) {
            fachhArrayName[i] = fachChoose[i].getName();
        }
        return fachhArrayName;
    }
}
