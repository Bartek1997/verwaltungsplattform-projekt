package main.dto;

import java.util.List;

public class TeilnehmerGruppeDto {
    private long gruppenId;
    private List<Long> teilnehmerAddId;
    private List<Long> teilnehmerRemoveId;

    public TeilnehmerGruppeDto(long gruppenId, List<Long> teilnehmerAddId, List<Long> teilnehmerRemoveId) {
        this.gruppenId = gruppenId;
        this.teilnehmerAddId = teilnehmerAddId;
        this.teilnehmerRemoveId = teilnehmerRemoveId;
    }

    public long getGruppenId() {
        return gruppenId;
    }

    public void setGruppenId(long gruppenId) {
        this.gruppenId = gruppenId;
    }

    public List<Long> getTeilnehmerAddId() {
        return teilnehmerAddId;
    }

    public void setTeilnehmerAddId(List<Long> teilnehmerAddId) {
        this.teilnehmerAddId = teilnehmerAddId;
    }

    public List<Long> getTeilnehmerRemoveId() {
        return teilnehmerRemoveId;
    }

    public void setTeilnehmerRemoveId(List<Long> teilnehmerRemoveId) {
        this.teilnehmerRemoveId = teilnehmerRemoveId;
    }
}
