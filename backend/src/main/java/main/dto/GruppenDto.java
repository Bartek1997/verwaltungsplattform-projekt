package main.dto;

import main.choose.FachChoose;
import main.choose.KategorieChoose;
import main.choose.RaumChoose;
import main.objects.Gruppen;
import main.objects.Teilnehmer;
import main.repository.GruppenRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class GruppenDto {
    private List<Gruppen> gruppen;

    private RaumChoose[] raumChoose;
    private String[] raumChooseName;

    private KategorieChoose[] kategorieChoose;
    private String[] kategorieChooseName;

    private FachChoose[] fachChoose;
    private String[] fachChooseName;

    public GruppenDto(List<Gruppen> gruppen, RaumChoose[] raumChoose, KategorieChoose[] kategorieChoose, FachChoose[] fachChoose) {
        this.gruppen = gruppen;
        this.raumChoose = raumChoose;
        this.kategorieChoose = kategorieChoose;
        this.fachChoose = fachChoose;
        this.raumChooseName = getRaumChooseName();
        this.kategorieChooseName = getKategorieChooseName();
        this.fachChooseName = getFachChooseName();
    }

    public List<Gruppen> getGruppen() {
        return gruppen;
    }

    public void setGruppen(List<Gruppen> gruppen) {
        this.gruppen = gruppen;
    }

    public RaumChoose[] getRaumChoose() {
        return raumChoose;
    }

    public KategorieChoose[] getKategorieChoose() {
        return kategorieChoose;
    }

    public FachChoose[] getFachChoose() {
        return fachChoose;
    }

    public String[] getRaumChooseName() {
        String[] raumarray = new String[raumChoose.length];
        for (int i = 0; i < raumarray.length; i++) {
            raumarray[i] = raumChoose[i].getName();
        }
        return raumarray;
    }
    public String[] getKategorieChooseName() {
        String[] raumarray = new String[kategorieChoose.length];
        for (int i = 0; i < raumarray.length; i++) {
            raumarray[i] = kategorieChoose[i].getName();
        }
        return raumarray;
    }
    public String[] getFachChooseName() {
        String[] raumarray = new String[fachChoose.length];
        for (int i = 0; i < raumarray.length; i++) {
            raumarray[i] = fachChoose[i].getName();
        }
        return raumarray;
    }


}
