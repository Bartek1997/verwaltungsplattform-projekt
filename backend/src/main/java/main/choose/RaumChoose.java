package main.choose;

public enum RaumChoose {
    R3 ("3 Kanrad Zuse"),
    R4("4 Exchange"),
    R5("5 Recharge"),
    R6("6 Cockpit"),
    R7("7 Grace Hopper"),
    R8("8 Explorer"),
    R9("9 Alan Turing"),
    R10("10 Radia Perlman"),
    R11("11 Linus Torvalds"),
    R12("12 Ada Lovelace"),
    R13("13 James Gosling"),
    R14("14 Margaret Hamilton");

    private final String name;

    RaumChoose(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

