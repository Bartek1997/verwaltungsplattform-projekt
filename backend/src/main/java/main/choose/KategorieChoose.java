package main.choose;

public enum KategorieChoose {
        NETZWERKTECHNIK ("Netzwerktechnik"),
        SOFTWAREENTWICKLER ("Softwareentwickler"),
        FIA ("FIA");


        private final String name;

        KategorieChoose(String name) {
                this.name = name;
        }

        public String getName() {
                return name;
        }
}
