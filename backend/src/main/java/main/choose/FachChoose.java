package main.choose;

public enum FachChoose {
    LINUX ("Linux"),
    WINDOWS ("Windows"),
    WEB ("Web"),
    JAVA ("Java"),
    DATENBANK ("Datenbanken");

    private final String name;

    FachChoose(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
