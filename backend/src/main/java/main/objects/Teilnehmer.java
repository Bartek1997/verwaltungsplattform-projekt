package main.objects;

import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;
@Entity
public class Teilnehmer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String vorname;
    private String nachname;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "gruppenId")
    private Gruppen gruppen;

    @Transient
    private long groupId;

    public Teilnehmer(){

    }

    public Teilnehmer(String vorname, String nachname) {
        this.vorname = vorname;
        this.nachname = nachname;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Gruppen getGruppen() {
        return gruppen;
    }

    public void setGruppen(Gruppen gruppen) {
        this.gruppen = gruppen;
    }

    public long getGroupId() {
        if (gruppen == null) {
            groupId = 0;
        }
        else{
            groupId = gruppen.getId();
        }

        return groupId;
    }
}
