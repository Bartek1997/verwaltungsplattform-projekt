package main.objects;

import com.fasterxml.jackson.annotation.JsonBackReference;
import main.choose.FachChoose;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String vorname;
    private String nachname;
    private String wohnort;
    private Double bruttogehalt;

    @Transient
    private Double nettogehalt;

    @Enumerated(EnumType.STRING)
    private FachChoose fach;

    ArrayList<Boolean> arbeitstage;


    public Trainer() {

    }

    public Trainer(String vorname, String nachname, String wohnort, Double bruttogehalt, FachChoose fach, ArrayList<Boolean> arbeitstage) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.wohnort = wohnort;
        this.bruttogehalt = bruttogehalt;
        this.fach = fach;
        this.arbeitstage = arbeitstage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getWohnort() {
        return wohnort;
    }

    public void setWohnort(String wohnort) {
        this.wohnort = wohnort;
    }

    public Double getBruttogehalt() {
        return bruttogehalt;
    }

    public void setBruttogehalt(Double bruttogehalt) {
        this.bruttogehalt = bruttogehalt;
    }

    public FachChoose getFach() {
        return fach;
    }

    public void setFach(FachChoose fach) {
        this.fach = fach;
    }

    public ArrayList<Boolean> getArbeitstage() {
        return arbeitstage;
    }

    public void setArbeitstage(ArrayList<Boolean> arbeitstage) {
        this.arbeitstage = arbeitstage;
    }


    public Double getNettogehalt() {
        if(bruttogehalt<1099.33){
            return bruttogehalt;
        }else if(bruttogehalt>1099.33 && bruttogehalt<1516){
            return bruttogehalt-(bruttogehalt*0.2);
        }else if(bruttogehalt>1516.00 && bruttogehalt<2599.33){
            return bruttogehalt-(bruttogehalt*0.325);
        }else if(bruttogehalt>2599.33 && bruttogehalt<5016.00){
            return bruttogehalt-(bruttogehalt*0.42);
        }else if(bruttogehalt>5016.00 && bruttogehalt<7516.00){
            return bruttogehalt-(bruttogehalt*0.48);
        }else if(bruttogehalt>7516.00 && bruttogehalt<83349.33){
            return bruttogehalt-(bruttogehalt*0.50);
        }else{
            return bruttogehalt-(bruttogehalt*0.55);
        }
    }

    public void setNettogehalt(Double nettogehalt) {
        this.nettogehalt = nettogehalt;
    }
}
