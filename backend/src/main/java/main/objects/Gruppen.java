package main.objects;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import main.choose.FachChoose;
import main.choose.KategorieChoose;
import main.choose.RaumChoose;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Entity(name = "gruppen")
public class Gruppen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    private KategorieChoose kategorie;


    @Enumerated(EnumType.STRING)
    private FachChoose unterrichtFachMo;

    @Enumerated(EnumType.STRING)
    private FachChoose unterrichtFachDi;

    @Enumerated(EnumType.STRING)
    private FachChoose unterrichtFachMi;

    @Enumerated(EnumType.STRING)
    private FachChoose unterrichtFachDo;

    @Enumerated(EnumType.STRING)
    private FachChoose unterrichtFachFr;

    @Enumerated(EnumType.STRING)
    private RaumChoose raumChoose;

    //private boolean qualifying;
    private int nummer;
    private LocalDate startDate;

    @Transient
    private LocalDate endDate;

    @Transient
    private String name;

    @Transient
    private String raumChooseName;


    @OneToMany(mappedBy = "gruppen")
    @JsonManagedReference
    private Set<Teilnehmer> teilnehmerSet = new HashSet<>();

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "trainerMo")
    private Trainer trainerMo;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "trainerDi")
    private Trainer trainerDi;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "trainerMi")
    private Trainer trainerMi;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "trainerDo")
    private Trainer trainerDo;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "trainerFr")
    private Trainer trainerFr;


    public Gruppen(){
    }

    public Gruppen(KategorieChoose kategorie, FachChoose unterrichtFachMo, FachChoose unterrichtFachDi, FachChoose unterrichtFachMi, FachChoose unterrichtFachDo, FachChoose unterrichtFachFr, RaumChoose raumChoose, int nummer, LocalDate startDate) {
        this.kategorie = kategorie;
        this.unterrichtFachMo = unterrichtFachMo;
        this.unterrichtFachDi = unterrichtFachDi;
        this.unterrichtFachMi = unterrichtFachMi;
        this.unterrichtFachDo = unterrichtFachDo;
        this.unterrichtFachFr = unterrichtFachFr;
        this.raumChoose = raumChoose;
        this.nummer = nummer;
        this.startDate = startDate;
    }

    private String createName(){
        if (kategorie.equals(KategorieChoose.NETZWERKTECHNIK)) {
            return "NT";
        } else if (kategorie.equals(KategorieChoose.SOFTWAREENTWICKLER)) {
            return "SW";
        } else if (kategorie.equals(KategorieChoose.FIA)){
            return "FIA";
        }
        else {
            return "";
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public KategorieChoose getKategorie() {
        return kategorie;
    }

    public void setKategorie(KategorieChoose kategorie) {
        this.kategorie = kategorie;
    }

    public FachChoose getUnterrichtFachMo() {
        return unterrichtFachMo;
    }

    public void setUnterrichtFachMo(FachChoose unterrichtFachMo) {
        this.unterrichtFachMo = unterrichtFachMo;
    }

    public FachChoose getUnterrichtFachDi() {
        return unterrichtFachDi;
    }

    public void setUnterrichtFachDi(FachChoose unterrichtFachDi) {
        this.unterrichtFachDi = unterrichtFachDi;
    }

    public FachChoose getUnterrichtFachMi() {
        return unterrichtFachMi;
    }

    public void setUnterrichtFachMi(FachChoose unterrichtFachMi) {
        this.unterrichtFachMi = unterrichtFachMi;
    }

    public FachChoose getUnterrichtFachDo() {
        return unterrichtFachDo;
    }

    public void setUnterrichtFachDo(FachChoose unterrichtFachDo) {
        this.unterrichtFachDo = unterrichtFachDo;
    }

    public FachChoose getUnterrichtFachFr() {
        return unterrichtFachFr;
    }

    public void setUnterrichtFachFr(FachChoose unterrichtFachFr) {
        this.unterrichtFachFr = unterrichtFachFr;
    }

    public RaumChoose getRaumChoose() {
        return raumChoose;
    }

    public void setRaumChoose(RaumChoose raumChoose) {
        this.raumChoose = raumChoose;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public String getName() {
        return nummer + " " + createName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStartDate() {
        LocalDate date = startDate;
        return date;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Set<Teilnehmer> getTeilnehmerSet() {
        return teilnehmerSet;
    }

    public void setTeilnehmerSet(Set<Teilnehmer> teilnehmerSet) {
        this.teilnehmerSet = teilnehmerSet;
    }

    public String getRaumChooseName() {
        return raumChoose.getName();
    }

    public Trainer getTrainerMo() {
        return trainerMo;
    }

    public void setTrainerMo(Trainer trainerMo) {
        this.trainerMo = trainerMo;
    }

    public Trainer getTrainerDi() {
        return trainerDi;
    }

    public void setTrainerDi(Trainer trainerDi) {
        this.trainerDi = trainerDi;
    }

    public Trainer getTrainerMi() {
        return trainerMi;
    }

    public void setTrainerMi(Trainer trainerMi) {
        this.trainerMi = trainerMi;
    }

    public Trainer getTrainerDo() {
        return trainerDo;
    }

    public void setTrainerDo(Trainer trainerDo) {
        this.trainerDo = trainerDo;
    }

    public Trainer getTrainerFr() {
        return trainerFr;
    }

    public void setTrainerFr(Trainer trainerFr) {
        this.trainerFr = trainerFr;
    }

    public LocalDate getEndDate() {
        if (kategorie.equals(KategorieChoose.NETZWERKTECHNIK)) {
            return startDate.plusMonths(6);
        } else if (kategorie.equals(KategorieChoose.SOFTWAREENTWICKLER)) {
            return startDate.plusMonths(6);
        } else if (kategorie.equals(KategorieChoose.FIA)){
            return startDate.plusMonths(12);
        }
        else {
            return startDate;
        }

    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
