package main.repository;

import main.objects.Gruppen;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GruppenRepository extends CrudRepository<Gruppen, Long> {

}
