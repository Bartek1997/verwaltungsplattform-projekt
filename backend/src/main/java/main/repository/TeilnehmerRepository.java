package main.repository;

import main.objects.Teilnehmer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeilnehmerRepository extends CrudRepository<Teilnehmer, Long> {

}
